insert into Product (SKU, Description, Price)
values ('sku-1', 'Conference T-shirt', 10),
       ('sku-2', 'Hoodie', 20),
       ('sku-3', 'No Wars T-shirt', 50),
       ('sku-4', 'Lifetime Unlimited Coffee', 199),
       ('sku-5', 'The Devil', 666)
