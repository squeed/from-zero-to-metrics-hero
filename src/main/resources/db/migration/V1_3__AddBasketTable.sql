CREATE TABLE Basket
(
    Id          BIGINT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    Country     VARCHAR(255),
    CreatedDate TIMESTAMP NOT NULL
);
