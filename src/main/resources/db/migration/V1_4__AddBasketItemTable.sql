CREATE TABLE BasketItem
(
    Id        BIGINT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    ProductId bigint not null
        constraint BASKETITEM_PRODUCT_ID_FK
            references PRODUCT,
    Quantity  bigint,
    BasketId  BIGINT
        constraint BASKETITEM_BASKET_ID_FK
            references BASKET
);
