package com.squeed.metricshol;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Order {

    private Long orderId;

    private List<OrderItem> orderItems = new ArrayList<>();

    private Long totalOrderValue;

    public Order() {
    }

    public Order(Basket basket) {
        orderId = basket.getId();
        orderItems = basket.getBasketItems().stream().map(OrderItem::new).collect(Collectors.toList());
        totalOrderValue = orderItems.stream().mapToLong(i -> i.getSum()).sum();
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public List<OrderItem> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(List<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }

    public Long getTotalOrderValue() {
        return totalOrderValue;
    }

    public void setTotalOrderValue(Long totalOrderValue) {
        this.totalOrderValue = totalOrderValue;
    }
}
