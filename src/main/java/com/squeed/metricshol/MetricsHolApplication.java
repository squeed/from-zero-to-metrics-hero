package com.squeed.metricshol;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;

@SpringBootApplication
public class MetricsHolApplication {

    private static final Logger logger = LoggerFactory.getLogger(MetricsHolApplication.class);
    private final ProductRepository productRepository;

    public static void main(String[] args) {
        SpringApplication.run(MetricsHolApplication.class, args);
    }

    public MetricsHolApplication(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @PostConstruct
    public void init() {
        logger.info("");
        logger.info("");
        logger.info("*** Product catalog ***");
        productRepository.findAll().forEach(p -> logger.info("    {}", p));

        logger.info("");
        logger.info("*** Actuator ***");
        logger.info("    http://localhost:8080/actuator");
        logger.info("");
        logger.info("");
        logger.info("*** Create basket ***");
        logger.info("    http://localhost:8080/create-basket/{country}");
        logger.info("    http://localhost:8080/create-basket/sweden");
        logger.info("");
        logger.info("*** Add to basket ***");
        logger.info("    http://localhost:8080/add-to-basket/{basketId}/{sku}");
        logger.info("    http://localhost:8080/add-to-basket/1/sku-1");
        logger.info("");
        logger.info("*** Checkout basket ***");
        logger.info("    http://localhost:8080/checkout/{basketId");
        logger.info("    http://localhost:8080/checkout/1");
        logger.info("");


    }
}
