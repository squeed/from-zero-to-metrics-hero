package com.squeed.metricshol;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@RestController
public class WebshopController {

    private final ProductRepository productRepository;
    private final BasketRepository basketRepository;

    public WebshopController(ProductRepository productRepository, BasketRepository basketRepository) {
        this.productRepository = productRepository;
        this.basketRepository = basketRepository;
    }

    @GetMapping(value = "products", produces = "application/json")
    public ResponseEntity<List<Product>> getProducts() {
        return ResponseEntity.ok(productRepository.findAll());
    }

    @GetMapping(value = "create-basket/{country}", produces = "application/json")
    public ResponseEntity<Basket> createBasket(@PathVariable("country") String country) {
        Basket basket = new Basket();
        basket.setCountry(country);

        return ResponseEntity.ok(basketRepository.save(basket));
    }

    @GetMapping(value = "add-to-basket/{basketId}/{sku}", produces = "application/json")
    public ResponseEntity<Basket> addToBasket(@PathVariable("basketId") Long basketId, @PathVariable("sku") String sku) {

        return basketRepository
            .findById(basketId)
            .map(basket -> {
                BasketItem basketItem = basket.getBasketItems().stream().filter(item -> item.getProduct().getSku().equals(sku)).findFirst().orElseGet(() -> createBasketItem(sku, basket));
                basketItem.setQuantity(basketItem.getQuantity() + 1);
                return ResponseEntity.ok(basketRepository.save(basket));
            }).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @GetMapping(value = "checkout/{basketId}", produces = "application/json")
    public ResponseEntity<Order> checkout(@PathVariable("basketId") Long basketId) {

        Optional<Basket> optionalBasket = basketRepository.findById(basketId);

        ResponseEntity<Order> result = optionalBasket
            .map(Order::new)
            .map(ResponseEntity::ok)
            .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));

        optionalBasket.ifPresent(basketRepository::delete);

        return result;
    }

    private BasketItem createBasketItem(String sku, Basket basket) {
        Product product = productRepository.findItemBySku(sku).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
        BasketItem basketItem = new BasketItem();
        basketItem.setProduct(product);
        basketItem.setQuantity(0L);

        basket.getBasketItems().add(basketItem);

        return basketItem;
    }

}
