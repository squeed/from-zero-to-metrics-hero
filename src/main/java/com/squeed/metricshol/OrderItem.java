package com.squeed.metricshol;

public class OrderItem {

    private Product product;
    private long quantity;

    public OrderItem() {
    }

    public OrderItem(BasketItem basketItem) {
        product = basketItem.getProduct();
        quantity = basketItem.getQuantity();
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public long getQuantity() {
        return quantity;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }

    public long getSum() {
        return product.getPrice() * getQuantity();
    }
}
