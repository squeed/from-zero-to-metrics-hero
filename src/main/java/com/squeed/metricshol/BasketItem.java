package com.squeed.metricshol;

import javax.persistence.*;

@Entity
@Table(name = "BasketItem")
public class BasketItem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    private Long id;

    @JoinColumn(name = "ProductId")
    @OneToOne(cascade = CascadeType.PERSIST)
    private Product product;

    private Long quantity;

    @Column(name = "Quantity")
    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "BasketItem{" +
            "id=" + id +
            ", product=" + product +
            ", quantity=" + quantity +
            '}';
    }
}
